"""Implementations of various algorithms to compute Optimal Limited Length Prefix Free Codes (and unit tests for those)  from unsorted weights.

- A first imlementation, corresponding to the classical algorithm =Package Merge=, is given in function =packageMergeOriginal=.
- A second implementation, corresponding to an adaptive variant of the latter, is given in function =packageMergeImproved=.

Those can be imported with instructions such as 
 from optimalLimitedLengthPrefixFreeCodes import packageMergeImproved,packageMergeOriginal

Original code by Pablo Rotondo, 
edited for testing and readability by Jeremy Barbay
"""

import unittest                 # So that examples of executions in the documentation are automatically validated.
import doctest                  # So that to build and automatically check a list of unit test of the algorithm
from huffman import huffman     # To compare the result with that of Huffman's algorithm when L is sufficicently large.
from functionsToTestPrefixFreeCodes import fibonacciSequence # Potentially the worst case for prefix free code computations.
from functionsToTestPrefixFreeCodes import NTimesEntropy # Useful functions 

import sys



# TODO:
# [X] DOCUMENT the format of the output in the documentation of the function "packageMergeSimplified"
# [X] MANAGE the case of an array of length 1 in  the function "packageMergeSimplified"
# [X] GENERALIZE algorithm to support repeated weights (PABLO)
# [X] COMPLETE test cases comparing the output of "packageMergeSimplified" with "huffman" (Fibonacci, quasi uniform and mixed)
# [ ] ADD to the packageMergeImproved a measure of "work performed" (using an external variable?)
# [ ] ADD test cases where the algorithm is faster than in the worst case for reasonably small values of L
# { } ADD UNIT TEST where L < huffman's code max length, and we know what the optimal code is (e.g. using original Package Merge algorithm, or the examples in the bibliography)
# [ ] ADD average code length function to the functionsToTestPrefixFreeCodes library.
# [ ] ADD test cases that use the original package merge that is implemented here
# [X] ADD Examples of execution to the docstrings of distinguishEquals(S)
# [X] ADD Examples of execution to the docstrings of merge(xs,ys) :
# [X] ADD Examples of execution to the docstrings of packageMergeOriginal(freqList,maxLen) and  packageMergeImproved(freqList,maxLen):
# [X] ADD Docstring and examples of execution to  codeFromDepthsDictionary
# [ ] ADD tests to verify that the codes return permit to build a binary tree with nodes of deg 0 or 2, equivalent to KraftSum == 1 .



##############################################################################
# Useful functions
####################################################################
def averageCodeLength(W,L,normalize=False) : 
    """Given a vector of weights and a proposed vector of code-words lengths
    it returns the average code word length. The algorithm sorts W and L accordingly,
    to multiply the largest from W to the smallest from L. See the examples.

    >>> averageCodeLength([5,1],[5,1])
    10
    
    >>> averageCodeLength([1,2,3,4],[2,2,2,2])
    20
    
    >>> averageCodeLength([1,2,3,4],[2,2,2,2],True)
    2
    
"""
    W.sort()
    L.sort()
    L.reverse()
    if normalize :
        return sum( w*l for (w,l) in zip(W,L) ) / sum(W) 
    else :
        return sum( w*l for (w,l) in zip(W,L) )

def projectDictionaryToVector(dictionary):
    """Given a dictionary {((w,r),l)}, returns the corresponding vector [l].

    >>> print(projectDictionaryToVector({(64, 0): 4, (32, 0): 4, (256, 0): 4, (2048, 0): 4, (16384, 0): 4, (128, 0): 4, (8, 0): 4, (1024, 0): 4, (2, 0): 4, (16, 0): 4, (32768, 0): 4, (1, 0): 4, (8192, 0): 4, (4096, 0): 4, (512, 0): 4, (4, 0): 4}))
    [4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4]
"""
    output = []
    for weight in dictionary:
        output.append(dictionary[weight])
    return output

def distinguishEquals(S) :
    """ Given a sorted list, enumerates equal terms by order of
        occurrence, i.e., if i is the j-th i in the list, then turn it into (i,j)
        for the output.

>>> S = [1,2,2,2,2,3,4]
>>> print(distinguishEquals(S))
[(1, 0), (2, 0), (2, 1), (2, 2), (2, 3), (3, 0), (4, 0)]

Note that the function does not verify that the list is sorted:
one can get strange results if it is not!

>>> S = [1,2,3,4,1,2,3,4]
>>> print(distinguishEquals(S))
[(1, 0), (2, 0), (3, 0), (4, 0), (1, 0), (2, 0), (3, 0), (4, 0)]
"""
    ans = [(S[0],0)]
    count = 0
    for i in xrange(1,len(S)) :
        if S[i] == S[i-1] :
            count += 1
        else :
            count = 0
        ans.append((S[i],count))
    return ans
    
def merge(xs,ys) :
    """Merge two sorted lists of pairs according to the first entry. 
    Does not remove duplicated elements.

>>> xs = [(1,0),(3,0),(5,0),(7,0),(8,0),(9,0)]
>>> ys = [(2,0),(4,0),(6,0),(8,0),(10,0)]
>>> print merge(xs,ys)
[(1, 0), (2, 0), (3, 0), (4, 0), (5, 0), (6, 0), (7, 0), (8, 0), (8, 0), (9, 0), (10, 0)]
"""
    ans = []
    i, j = 0, 0
    while i < len(xs) and j < len(ys) :
        if xs[i][0] <= ys[j][0] :
            ans.append(xs[i])
            i += 1
        else :
            ans.append(ys[j])
            j += 1
    if i < len(xs) :
        ans += xs[i:]
    if j < len(ys) :
        ans += ys[j:]
    return ans

def codeFromDepthsDictionary(depthsDictionary) :
    """Compute a vector of prefix free code-words from a vector of depths
    satisfying EQUALITY in the Kraft inequality.
     
    >>> codeProfile = { 'a' : 1 , 'b' : 2, 'c' : 3, 'd' : 4, 'e': 4}
    >>> print(codeFromDepthsDictionary(codeProfile))
    [('e', '0000'), ('d', '0001'), ('c', '001'), ('b', '01'), ('a', '1')]

    >>> codeProfile = { (1,0) : 4 , (1,1) : 4, (1,2) : 4, (1,3) : 4}
    >>> print(codeFromDepthsDictionary(codeProfile))
    [((1, 3), '0000'), ((1, 2), '0001'), ((1, 1), '0010'), ((1, 0), '0011')]
     """
    s = [ (d,k) for (k,d) in depthsDictionary.items() ]
    
    # if already sorted, like in our case, no need for the next step
    s.sort() 
    s.reverse()
    # construct
    L = s[0][0]
    acc = 0
    ans = []
    
    for d,k in s :
        diff = L-d
        ans.append((k,format(acc, 'b').zfill(L)[:d]))
        acc += (1<<diff)
    return ans
    
    
def findIndex(xs,p) :
    i = 1
    n = len(xs)
    while i <= n and  xs[i-1] <= p :
        i = i<<1
    l = 0
    h = min(n,i-1)
    while h - l > 1 :
        m = (h+l)>>1
        if xs[m] <= p :
            l = m
        else :
            h = m
    return l + 1
    
def mergeFast(xs,ys) :
    """Merge two sorted lists of pairs according to the first entry. 
    Does not remove duplicated elements. Returns also the lists of lengths of blocks of xs and ys
    that are interwined in the answer.
"""
    ans = []
    blocksX  = []
    blocksY  = []
    i, j = 0, 0
    while i < len(xs) and j < len(ys) :
        if xs[i] <= ys[j] :
            ix = findIndex(xs[i:],ys[j])
            ans += xs[i:(i+ix)]
            blocksX.append(ix)
            i += ix
        else :
            jy = findIndex(ys[j:],xs[i])
            ans += ys[j:(j+jy)]
            blocksY.append(jy)
            j += jy
            
    if i < len(xs) :
        blocksX.append(len(xs)-i)
        ans += xs[i:]
    if j < len(ys) :
        blocksY.append(len(ys)-j)
        ans += ys[j:]
    return ans,blocksX,blocksY
    
    
def package(xs) :
    ans = []
    n = len(xs)
    for j in xrange(1,n,2) :
        ans.append( (xs[j-1][0]+xs[j][0],
                                        xs[j-1][1]+xs[j][1]))
    return ans



############################################################################## Algorithms to compute Optimal Limited Length Prefix Free Codes
##############################################################################

def packageMergeOriginal(freqList,maxLen,countBlocks=False) :
    """Original package-merge algorithm
       Returns a sorted list of code-lengths

    >>> W = [1,1,1,1]
    >>> print(packageMergeOriginal(W,3))
    [2, 2, 2, 2]
    
    >>> W = [1,2,4,8,16,32,64,128]
    >>> print(packageMergeOriginal(W,3))
    [3, 3, 3, 3, 3, 3, 3, 3]
    
    >>> W = [1,2,4,8,16,32,64,128]
    >>> print(packageMergeOriginal(W,4))
    [1, 3, 4, 4, 4, 4, 4, 4]
    """
    blocksFromPackages = []
    blocksFromInitialEls = []
    
    
    if len(freqList) == 1 :
        return [0]
        
    freqList.sort() 
    auxFreqList  = distinguishEquals(freqList)
    
    n = len(freqList)
    maxElements = 2*n-2
    
    currentList = [ (element[0],[element]) for element in auxFreqList]
    initialList =   [ (element[0],[element]) for element in auxFreqList]
    
    for i in xrange(1,maxLen) :
        
        # package the activeList
        packageList = package(currentList)
        
        # merge with the frozenList into the activeList
        currentList,bX,bY = mergeFast(packageList,initialList)
        
        blocksFromPackages += [bX]
        blocksFromInitialEls += [bY]
        
    assert(len(currentList)<2*n)
        
    currentList = currentList[:maxElements]
    
    # use the lists (second entries) to produce the depths
    depths = {}
    for s in auxFreqList :
        depths[s] = 0
    for tups in currentList :
        for s in tups[1] :
            depths[s] = depths[s] + 1
    
    output = sorted(depths.values())        
    
    if countBlocks : 
        return output, blocksFromPackages, blocksFromInitialEls
    
    return output

nbLoopIterations = 0

        
    
def packageMergeImproved(freqList,maxLen,
    optArgs={}):
    """Compute an optimal prefix free code among codes of maximal length L with weights S (improved version)
       Return a sorted list of code-lengths


    >>> W = [1,1,1,1]
    >>> print(packageMergeImproved(W,3))
    [2, 2, 2, 2]
    
    >>> W = [1,2,4,8,16,32,64,128]
    >>> print(packageMergeImproved(W,3))
    [3, 3, 3, 3, 3, 3, 3, 3]
    
    >>> W = [1,2,4,8,16,32,64,128]
    >>> print(packageMergeImproved(W,4))
    [1, 3, 4, 4, 4, 4, 4, 4]
    
    >>> W = [6,7,8,9,10,11]
    >>> print(packageMergeImproved(W,3))
    [2, 2, 3, 3, 3, 3]
    
    >>> W = [10,11,12,13,14,15,16,17,18,19]
    >>> print(packageMergeImproved(W,4))
    [3, 3, 3, 3, 3, 3, 4, 4, 4, 4]
    """
    opts=dict(mainLoopCount=False,countBlocks=False)
    opts.update(optArgs)

    if len(freqList)==1:
        return [0]
        
    freqList.sort() 
    auxFreqList  = distinguishEquals(freqList)

    n = len(freqList)
    maxElements = 2*n-2
    
    # auxiliary counting variables
    cnt = 0
    blocksFromPackages = []
    blocksFromInitialEls = []
    
    # fixed forever list
    blueIndex = 0 
    # active list
    redIndex  = 2
    # latest merges that are not already in the previous lists
    currentList = [ (element[0],[element]) for element in auxFreqList]
    # list of original elements
    initialList =   [ (element[0],[element]) for element in auxFreqList]    
    remainingInitialIndex = 2

    for i in xrange(1,maxLen) :
        cnt += 1
        
        
        packages = package(currentList[blueIndex:])
        merges,blocksX,blocksY   = mergeFast(packages,initialList[remainingInitialIndex:])
        evenReds = redIndex - (redIndex%2)
        lastPackageOfReds    = (currentList[evenReds-2][0] + currentList[evenReds-1][0],
                                        currentList[evenReds-2][1] + currentList[evenReds-1][1])

        indexLastRedPackage  = merges.index(lastPackageOfReds) # exact index ! it is present
        
        remainingInitialIndex = findIndex(initialList,lastPackageOfReds)
        
        currentList = currentList[:redIndex] + merges
        
        blueIndex = redIndex - (redIndex%2)
        redIndex  += indexLastRedPackage + 1
        if ( (redIndex-blueIndex) == 1 
                and redIndex < len(currentList) ) :
            redIndex += 1
            remainingInitialIndex += 1
        
        if opts['countBlocks'] : 
            blocksFromPackages += [blocksX]
            blocksFromInitialEls += [blocksY]
        
        if redIndex >= maxElements : 
            break
    
    # veryfying length of the final list
    # before cutting the last element
    assert(len(currentList)<2*n)

    currentList = currentList[:maxElements]

    # use the lists (second entries) to produce the depths
    depths = {}
    for s in auxFreqList :
        depths[s] = 0
    for tups in currentList :
        for s in tups[1] :
            depths[s] = depths[s] + 1
    output = sorted(depths.values())
    
    
    if opts['mainLoopCount'] and opts['countBlocks'] :
        return (output, cnt, blocksFromPackages,
                    blocksFromInitialEls)
    
    if opts['mainLoopCount'] :
        return output, cnt
        
    if opts['countBlocks'] :
        return (output, blocksFromPackages,
                 blocksFromInitialEls)
#        optionals.append( (blocksFromPackages,blocksFromInitialEls)
        
    return output
    
##############################################################################
# Unit Tests for Optimal Limited Length Prefix Free Code computation
##############################################################################

listOfAlgorithmsToTest = [packageMergeOriginal,packageMergeImproved]

def test(weights,maxCodeLength,expectedOutput):
    failed = False
    for algo in listOfAlgorithmsToTest:
        #result = projectDictionaryToVector(algo(weights,maxCodeLength))
        result = algo(weights,maxCodeLength)
        if result != expectedOutput:
            failed = True
            print("\nTEST FAILED: \n  Algorithm `"+str(algo)
                  +"' \n  on input ("+str(weights)+", "+str(maxCodeLength)+")"
                  +"' \n  gave '"+str(result)
                  +"' \n  instead of the expected '"+str(expectedOutput)+"'!")
    assert(not failed)
    
class OptimalBoundedLenghtPrefixFreeCodesTest(unittest.TestCase):
    """Basic tests for algorithms computing optimal prefix free codes for a given maximal codelength.
    """
    
    def testSingleWeight(self):
        """List with a single weight."""
        W = [1]
        L = 1
        answerExpected = [0]
        test(W,L,answerExpected)
        
    def testPairOfWeights(self):
        """List with only two weights."""
        W = [1,1]
        L = 1
        answerExpected = [1,1]
        test(W,L,answerExpected)
        
    def testComparisonWithHuffmanOnUniformWeightsForBigValuesOfL(self):
        """Compare with Huffman on uniform sequence for big enough values of L."""
        size = 2
        W = [5]*size
        huffmanAverageCodeLength = averageCodeLength(W,huffman(W))
        packageMergeAvergeCodeLength = averageCodeLength(W,packageMergeImproved(W,size)) 
        self.assertEqual(huffmanAverageCodeLength,packageMergeAvergeCodeLength)
        
    def testComparisonWithHuffmanOnQuasiUniformWeightsForBigValuesOfL(self):
        """Compare with Huffman on uniform sequence for big enough values of L."""
        size = 16
        offset = 2**8
        W = []
        for i in range(size):
            W.append(offset+i)
        huffmanAverageCodeLength = averageCodeLength(W,huffman(W))
        packageMergeAvergeCodeLength = averageCodeLength(W,packageMergeImproved(W,size)) 
        self.assertEqual(huffmanAverageCodeLength,packageMergeAvergeCodeLength)
        
    def testComparisonWithHuffmanOnFibonaccyForBigValuesOfL(self):
        """Compare with Huffman on (a variant of the) Fibonacci sequence (with no repeated values) for big enough values of L."""
        size = 8
        W = fibonacciSequence(size,[1,2])
        huffmanAverageCodeLength = averageCodeLength(W,huffman(W))
        packageMergeAvergeCodeLength = averageCodeLength(W,packageMergeImproved(W,size)) 
        self.assertEqual(huffmanAverageCodeLength,packageMergeAvergeCodeLength)
        
    def testComparisonWithHuffmanOnSeveralFibonaccySuitesForBigValuesOfL(self):
        """Compare with Huffman on various Fibonacci sequences concatenated (but with no repeated values) for big enough values of L."""
        ## Complicated construction of the instance (trying to get all values distinct make things complicated!)
        sizeOfEachFibonacciSequence = 5
        nbOfFibonacciSequences = 2
        size = sizeOfEachFibonacciSequence * nbOfFibonacciSequences
        basicFibonacciSequence = fibonacciSequence(sizeOfEachFibonacciSequence,[2**nbOfFibonacciSequences,2**nbOfFibonacciSequences+2])
        W = []
        for i in range(nbOfFibonacciSequences):
            for j in range(len(basicFibonacciSequence)):
                basicFibonacciSequence[j] += 1
            W = W + basicFibonacciSequence
        assert(len(W)==size)
        # Computation of Huffman Code
        huffmanCodeLengths = huffman(W)
        # Computation of the package Merge Code
        packageMergeCodeLengths = packageMergeImproved(W,size) # Using size as the maximal code length, the code should be equal to Huffman's

        self.assertEqual(averageCodeLength(W,huffmanCodeLengths),averageCodeLength(W,packageMergeCodeLengths))

        
    def testComparisonWithOriginalPackageMergeQuasiUniformInput(self):
        """Compare with original package-merge algorithm : quasi-uniform case"""
        nbOfElements = 10
        W = [ i for i in xrange(nbOfElements,2*nbOfElements) ]
        size = 10
        
        packageMergeImprovedCodeLengths = packageMergeImproved(W,size)
        codeLengthPackageMergeImproved = averageCodeLength(W,packageMergeImprovedCodeLengths)
        
        packageMergeCodeLengths = packageMergeOriginal(W,size)
        codeLengthPackageMerge = averageCodeLength(W,packageMergeCodeLengths)
        
        self.assertEqual(codeLengthPackageMergeImproved,codeLengthPackageMerge)
        
    def testComparisonWithGeometricInputLowMaxLength(self):
        """Geometric input but small maximum length"""
        size = 4
        nbOfElements = 2**4
        W = [ 2**i for i in xrange(nbOfElements) ]
        answer = [size for i in xrange(nbOfElements)]
        test(W,size,answer)
        
def main():
    unittest.main()
if __name__ == '__main__':
    doctest.testmod()
    main()
