from optimalLimitedLengthPrefixFreeCodes import packageMergeImproved, packageMergeOriginal

# returns the floor of the base 2 logarithm of a non-negative integer n
def log2(n) :
    if n <= 1 :
        return 0
    return len(bin(n)) - 2


def showCaseQuasiUniformInput() :
    """Shows the number of main loop iterations
        for the quasi-uniform case: for n elements, the number of
        main loop iterations equals 1+floor(log(n-2,2)) .
    """
    print "======= show-casing quasi-uniform input ======="
    for nbOfElementsRun in xrange(2,20) :
        W = [ i for i in xrange(nbOfElementsRun,2*nbOfElementsRun)]
        lg2 = log2(nbOfElementsRun) # lg2 + 1 is the minimal max length in any case
        packageMergeImprovedCodeLengths,count = packageMergeImproved(W,lg2+1,
                                    dict(mainLoopCount=True))
        print "Number of Elements = ",nbOfElementsRun, " -- " , "Iterations of main loop: ", count, " -- ", "Weight vector: ", W
    print "======= finished show-casing quasi-uniform input ======="
        
def showCaseTwoQuasiUniformRuns(nbOfElementsRun) :
    """Shows the number of main loop iterations
        for an input made up of two quasi-uniform runs,
        and we show the incidence of the gap (relative to the length of the runs)
        on the number of main loops iterations.
        
        The predicted performance is : 
        lg2(length first run) + lg2(length second run) + 1 +  [ 1 - sum(elements first run) /(smallest second run)]
    """
    print "======= show-casing overlapping quasi-uniform runs ======="
    
    print "Gap equal to sum of two largest of the first run: "
    nbOfElementsRun = 10
    minimal = 60
    run1 = [minimal+i-nbOfElementsRun for i in xrange(nbOfElementsRun,2*nbOfElementsRun)]
    gap =  sum(run1[-2:])
    run2 = map(lambda a : a + gap, run1)
    W = run1+run2
    cost1 = log2(len(run1)-2)
    cost2 = log2(len(run2)-2)
    print "Input vector: ", W
    print "Sum of the costs of the runs = ", cost1+cost2+1
    packageMergeImprovedCodeLengths,count = packageMergeImproved(W,10,
                                    dict(mainLoopCount=True))
    print "Real number of iterations = ",count
    print ""
    
    print "Gap equal to sum all elements of the first run: "
    gap = sum(run1) 
    run2 = map(lambda a : a + gap, run1)
    W = run1+run2
    cost1 = log2(len(run1)-2)
    cost2 = log2(len(run2)-2)
    print "Input vector: ", W
    print "Sum of the costs of the runs = ", cost1+cost2+1
    packageMergeImprovedCodeLengths,count = packageMergeImproved(W,20,
                                    dict(mainLoopCount=True))
    print "Real number of iterations = ",count
    print ""
    
    
    print "Gap equal to half the sum all elements of the first run: "
    gap = sum(run1)  / 2
    run2 = map(lambda a : a + gap, run1)
    W = run1+run2
    cost1 = log2(len(run1)-2)
    cost2 = log2(len(run2)-2)
    print "Input vector: ", W
    print "Sum of the costs of the runs = ", cost1+cost2+1
    packageMergeImprovedCodeLengths,count = packageMergeImproved(W,20,
                                    dict(mainLoopCount=True))
    print "Real number of iterations = ",count
    print ""
    
    print "Gap equal to 1/3 of the sum all elements of the first run: "
    gap = sum(run1)  / 3
    run2 = map(lambda a : a + gap, run1)
    W = run1+run2
    cost1 = log2(len(run1)-2)
    cost2 = log2(len(run2)-2)
    print "Input vector: ", W
    print "Sum of the costs of the runs = ", cost1+cost2+1
    packageMergeImprovedCodeLengths,count = packageMergeImproved(W,20,
                                    dict(mainLoopCount=True))
    print "Real number of iterations = ",count
    print ""
    
    print "Gap equal to 1/4 of the sum all elements of the first run: "
    gap = sum(run1)  / 4
    run2 = map(lambda a : a + gap, run1)
    W = run1+run2
    cost1 = log2(len(run1)-2)
    cost2 = log2(len(run2)-2)
    print "Input vector: ", W
    print "Sum of the costs of the runs = ", cost1+cost2+1
    packageMergeImprovedCodeLengths,count = packageMergeImproved(W,20,
                                    dict(mainLoopCount=True))
    print "Real number of iterations = ",count
    
    print "======= finished show-casing overlapping quasi-uniform runs ======="

# need a more precise version : guess, we need to compare partial some of the first run
# to the smallest elements of the second
#def estimateCost(W) :
#    m = W[0]
#    runs = []
#    current = [m]
#    for i in xrange(1,len(W)) :
#        if W[i] <= 2*m :
#            current.append(W[i])
#        else :
#            runs.append(current)
#            m = W[i]
#            current = [m]
#    runs.append(current)
#    
#    cost = 1 + log2(len(runs[0])-2) +  sum(log2(len(run)) for run in runs[1:])
#    for i in xrange(1,len(runs)) :
#        cost -= sum(runs[i-1]) / runs[i][0]
#    return cost

def showCaseHandMadeExamples() :
    """Shows the number of main loop iterations

    """
    W = [64,64,65,65,130,131,132]
    print "Input vector: ", W
#   print "Estimated cost = ", estimateCost(W)
    packageMergeImprovedCodeLengths,count = packageMergeImproved(W,20,
                                    dict(mainLoopCount=True))
    print "Real number of iterations = ",count
    
    W = [i for i in xrange(128,201)] + [i for i in xrange(256,471)]
    print "Input vector: ", W
#    print "Estimated cost = ", estimateCost(W)
    packageMergeImprovedCodeLengths,count = packageMergeImproved(W,20,
                                    dict(mainLoopCount=True))
    print "Real number of iterations = ",count
    
    W = [i for i in xrange(128,201)] + [i for i in xrange(400,471)]
    print "Input vector: ", W
#    print "Estimated cost = ", estimateCost(W)
    packageMergeImprovedCodeLengths,count = packageMergeImproved(W,20,
                                    dict(mainLoopCount=True))
    print "Real number of iterations = ",count
    
    W = [i for i in xrange(128,201)] + [i for i in xrange(1024,1030)]
    print "Input vector: ", W
    packageMergeImprovedCodeLengths,count = packageMergeImproved(W,20,
                                    dict(mainLoopCount=True))
    print "Real number of iterations = ",count
    
    

def benchmark(W,L) :
    print "Input vector: ", W
    codelengths,count,bX,bY = packageMergeImproved(W,L,
                                    dict(mainLoopCount=True,countBlocks=True))
#    print "Code lengths: ", codelengths
    print "----- now for the pakcage merge improved ----- "
#    print "Real number of iterations = ",count
#    print "Blocks involved in merges ...."
#    print "Blocks corresponding to packages : ", len(bX) 
#    print "values: ", bX
#    print "Blocks corresponding to initial elements : ", len(bY) 
#    print "values: ", bY
    bX = reduce(lambda x,y : x+y, bX)
    bY = reduce(lambda x,y : x+y, bY)
    sm = sum(map(log2,bX)) + sum(map(log2,bY))
    print "Total sum of logs of blocks", sm
    print "----- now for the original pakcage merge ----- "
    _,bX,bY = packageMergeOriginal(W,L,True)
#    print "Blocks involved in merges ...."
#    print "Blocks corresponding to packages : ", len(bX) 
#    print "values: ", bX
#    print "Blocks corresponding to initial elements : ", len(bY) 
#    print "values: ", bY
    bX = reduce(lambda x,y : x+y, bX)
    bY = reduce(lambda x,y : x+y, bY)
    sm = sum(map(log2,bX)) + sum(map(log2,bY))
    print "Total sum of logs of blocks", sm
    print "==============================================="
    

    
def showMergingBlocks() :
    """Shows the number of main loop iterations
        as well as the sizes of blocks involved in merges of the improved
            algorithm.
        TODO. compare with the basic one...
    """
    W = [64,64,65,65,130,131,132]
    benchmark(W,20)
    
    W = [i for i in xrange(128,201)] + [i for i in xrange(256,471)]
    benchmark(W,20)
    
    W = [i for i in xrange(128,201)] + [i for i in xrange(400,471)]
    benchmark(W,20)
    
    W = [i for i in xrange(128,201)] + [i for i in xrange(1024,1030)]
    benchmark(W,20)
    
    import random
    random.seed(8)
    n = 7
    W = [random.randint(i/2+1,i+2) for i in xrange(2**n)] 
    benchmark(sorted(W),n+2)
    
#showCaseQuasiUniformInput() 

#showCaseTwoQuasiUniformRuns(10)

#showCaseHandMadeExamples()

showMergingBlocks()
