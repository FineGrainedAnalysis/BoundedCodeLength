#+OPTIONS: ':nil *:t -:t ::t <:t H:3 \n:nil ^:t arch:headline author:t c:nil creator:comment d:(not "LOGBOOK") date:t e:t email:nil f:t inline:t num:t p:nil pri:nil prop:nil stat:t tags:t tasks:t tex:t timestamp:t toc:nil todo:t |:t
#+TITLE: Faster Optimal Limited Length Prefix Free Codes (Sometimes)
#+AUTHOR: Jérémy Barbay, Thierry Lecroq
#+EMAIL: jeremy@barbay.cl
#+DESCRIPTION: Adaptive (Analysis and Design of) an Algorithm to compute optimal prefix free codes with bounded code length 
#+KEYWORDS: Adaptive, Huffman, Optimal Prefix Free codes
#+LANGUAGE: en
#+SELECT_TAGS: export
#+EXCLUDE_TAGS: noexport
#+CREATOR: Emacs 24.4.1 (Org mode 8.2.5h)
#+OPTIONS: texht:t
#+DATE: \today
#+LATEX_CLASS: article
#+LATEX_CLASS_OPTIONS:
#+LATEX_HEADER:
#+LATEX_HEADER_EXTRA: \usepackage{fullpage}

#+begin_abstract  
#+end_abstract 

* Introduction

- Optimal Prefix Free Codes (aka Huffman) have many applications in compression since their proposal in 1952. 

- More recently, they have applications in the design of compressed Data Structures, where their height must be bounded: designing optimal prefix free codes under such constraint is know since (...) as the problem of computing Optimal Limited Length Prefix Free Codes.
  - Fixed L
  - Fixed l_1,\ldots, l_n

- Past Results (...)

- Observations
  1. L = log n -> Solution found in O(n) time, better than O(nL)
  2. if $L > \lambda \in \omega(\lg n)$, where $\lambda$ is the minimal height of an optimal prefix free code (with no constraints), such a constrained optimal prefix free code can be found in time within $O(n\lg n)$, potentially much smaller than $O(nL)$
     - costs nothing asymptotically to compute huffman code (same cost as sorting) before OLLPFC.
  3. in LH algorithm, L is used only as a threshold to interrupt the computation
 
- Questions

  1. Is there a simpler algorithm (i.e. not depending on a reduction)?
  2. Is O(nL) time with O(L) space optimal in the worst case over instances of n weights under the constraint max L[i] \leq L?
  3. Is there an algorithm which takes more advantage of the value of $L$, for instance such that to achieve a cost comparable to the computation of unconstrained OPFC when $L$ is very big, or $O(n)$ when $L$ is very small.

- Results

  1. Simpler Algorithm
  2. Proof of fastness on "large" classes of instances
  3. Computation lower bound (adaptive and classical)

* A Simpler Algorithm
  - Fusioning Packaging and Merging phases
  - Definition: NodeSet (Forest = list of nodesets)
  - Theorem: Correct output
    - Lemma:
      - at time $\lg n$, algorithm yields (optimal) casi-uniform code length
    - Lemma: 
      - Forest(W,L) => output optimal (W,L)
      - implies
      - Forest(W,L+1) => output optimal (W,L+1)
  - Claim/Corrolary of future theorem: O(nL)
* Fine Grained Analysis
  - Lemma: Congelation
  - Definition: \delta
  - Theorem: O(n\delta) time, O(n) space
* Computational Lower Bound
  - Lemma: $\Omega(n\lg n)$ already when $L=1+\lg n$ 
* Discussion
  + finer constraints (one code length limit per message)
  + alphabetic binary search tree (with constraints)
  + Dynamic trees
  + D-ary for D>2
  + Computation on Unsorted input (via Deferred Data Structures)
  + Applications to Compressed Data Structures for Permutations
    - bounded height => bounded worst case query time
  + Experimentations 
    - permutations from BWT (Construction time, Indexing space, Query Time)
