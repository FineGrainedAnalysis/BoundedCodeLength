\section{Introduction}

% Optimal Prefix Free Codes%
Given $n$ positive weights $\weight{1..n}$, coding\begin{LONG}\footnote{We note $[i..j]=\{i,i+1,\ldots,j\}$ the integer range from $i$ to $j$, and $A[i..j]=\{A[i],A[i+1],\ldots,A[j]\}$ the set of values of an array $A$ which indices occupy this range.}\end{LONG} for the frequencies $\left\{{\weight{i}}/{\sum_{j=1}^n\weight{j}}\right\}_{i\in[1..n]}$ of $n$ messages\begin{LONG}\footnote{Huffman~\cite{1952-IRE-AMethodForTheInstructionOfMinimumRedundancyCodes-Huffman} introduced the terminology of \emph{messages} as input and \emph{symbols} as output, which should not be confused with the terminology of \emph{input symbols}, \emph{letters} or \emph{words} for the input and \emph{output symbols} or \emph{bits} in the binary case.}\end{LONG}, and a number $\sigma$ of output symbols,
%
an \textsc{Optimal Prefix Free Code}~\cite{2006-WILEY-ElementsOfInformationTheory-CoverThomas} is a set of $n$ strings on the alphabet $[1..\sigma]$, of variable lengths $\codeLength{1..n}$ such that no string is prefix of another, and the average length of the code (the set) is minimized (i.e. $\sum_{i=1}^n\codeLength{i}\weight{i}$ is minimal).
%
\begin{LONG}
The peculiarity of such codes is that even though the code strings assigned to the messages can differ in length (assigning shorter ones to more frequent messages yields a smaller average $\sum_{i=1}^n\codeLength{i}\weight{i}$ for output symbols), the prefix free property ensures a non-ambiguous and online decoding.
\end{LONG}
%
The algorithm originally suggested by Huffman~\cite{1952-IRE-AMethodForTheInstructionOfMinimumRedundancyCodes-Huffman} computes such code in time $O(n\lg n)$.
\begin{LONG}
When the weights are already sorted, van Leeuwen~\cite{1976-ICALP-OnTheConstructionOfHuffmanTrees-Leeuwen} improved this result to $O(n)$, 
and Moffat and Turpin~\cite{1998-TIT-EfficientConstructionOfMinimumRedundancyCodesForLargeAlphabets-MoffatTurpin} further improved this result when the weights are both sorted and compressed into $r$ runs of equal values to within $O(r\log n/r)\subset O(n)$. When the weights are not sorted, Belal and Elmasry~\cite{2006-STACS-DistributionSensitiveConstructionOfMinimumRedundancyPrefixCodes-BelalElmasry} suggested an algorithm running in time within $O(n\kappa)$ where $\kappa$ is the minimal number of distinct codelengths in any optimal code, and Barbay~\cite{2020-Algorithms-OptimalPrefixFreeCodesWithPartialSorting-Barbay} described an algorithm running in time within $O(n\lg\alpha)$ where $\alpha$ is the number of times that Huffman's algorithm alternates between picking External and Internal nodes.
\end{LONG}

% Optimal Limited Length Prefix Free Code
More recently, such codes have found applications in the design of compressed Data Structures~\cite{2013-TCS-CompressedRepresentationsOfPermutationsAndApplications-BarbayNavarro}, where their height corresponds to the query support time in the worst case, and as such must be bounded. The problem of designing optimal prefix free codes under such constraint first appeared in 1971~\cite{1971-IEEETransInfTheory-CodesBasedOnInnaccurateSourceProbabilities-Gilbert}, % 1972~\cite{1972-SIAM-PathLengthOfBinarySearchTrees-HuTan}
and it is known as that of computing an \textsc{Optimal Limited Length Prefix Free Code} given a sorted array of $n$ weights and an integer bound $L>\lg n$ on the codelengths of the resulting code.
%
In 1972, Hu and Tan~\cite{1972-SIAM-PathLengthOfBinarySearchTrees-HuTan} gave a dynamic programming algorithm for finding such a code in time exponential in the input size $n$.
%
Two years later, using a different approach, Garey~\cite{1974-SIAM-OptimalBinarySearchTreesRestricted-Garey} gave another dynamic programming algorithm requiring time and space within $O(n^2L)$.
%
The next improvement came 13 years later, when Larmore~\cite{1987-SIAM-HeightRestrictedOptimalBinaryTrees-Larmore} suggested in 1987 a dynamic program combining both methods into one running in time $O(n^{\frac{3}{2}}L\lg^{\frac{1}{2}} n)$, while using space within $O(n^{\frac{3}{2}}L\lg^{-\frac{1}{2}} n)$.
%
In 1990, Larmore and Hirschberg~\cite{1990-AFastaAlgorithmForOptimalLengthLimitedHuffmanCodes-LarmoreHirschberg} proposed a complicated \texttt{Package and Merge} algorithm (based on a reduction to the \textsc{Coin's collector} problem) running in time within $O(nL)$ using space within $O(n)$, which Golin and Zhang~\cite{2010-IEEETransInfTheory-DynamicProgrammingLLHC-GolinZhang} simplified to an optimized dynamic program running in the same time and space in 2010.
%
 Those time and space were improved in two directions in 1995 when Katajainen et al.~\cite{1995-FastSpaceEcoAlgorithmLLC-KatajainenMoffatTurpin} improved the space within $O(n)$ to space within $O(L^2)$ (with the same running time within $O(nL)$ as Larmore and Hirschberg's algorithm~\cite{1990-AFastaAlgorithmForOptimalLengthLimitedHuffmanCodes-LarmoreHirschberg}), while Schieber~\cite{1998-JournalAlgorithms-Schieber} improved the running time to within $O(n\times2^{O(\sqrt{\lg L \lg\lg n})})$ (with the same space within $O(n)$ as Larmore and Hirschberg's algorithm~\cite{1990-AFastaAlgorithmForOptimalLengthLimitedHuffmanCodes-LarmoreHirschberg}).

%% Questions 
All those results correspond to worst cases over instances of fixed size $n$ and parameter $L$,
yet in many cases the \textsc{Optimal Limited Length Prefix Free Code} of an instance can be computed much faster. 
Trivial examples where such codes can be compute in time linear in the input size $n$ are
% \begin{enumerate}
% \item
1) quasi uniform sequences~\cite{2013-TIT-OptimalPrefixCodesForPairsOfGeometricallyDistributedRandomVariables-BassinoClementSeroussiViola} (see Section~\ref{sec:background} for the formal definition) when all the weights are within a factor of two of one another (i.e., $\max_i W[i] \leq 2 \min_i W[i]$); and
% \item
2) instances where $L$ is larger than the maximal codelength $\lambda(W)$ of an unconstrained optimal prefix free code for the weights $W$ (using van Leeuwen's algorithm~\cite{1976-ICALP-OnTheConstructionOfHuffmanTrees-Leeuwen} on the sorted weights).
% \end{enumerate}
%
\textbf{Are there instances of intermediate difficulty between the worst case and such ``easy'' instances, and is there an algorithm taking advantage of those while still running in time within a constant factor of the optimal in the worst case over instances formed by $n$ weights with parameter $L$?}

%% Our results
After describing more formally the background of our results (in Section~\ref{sec:background}), we describe a new algorithm which is directly processing the weights (as opposed to the state of the art reduction to the \textsc{Coin Collector Problem}~\cite{1990-AFastaAlgorithmForOptimalLengthLimitedHuffmanCodes-LarmoreHirschberg}, and which detects ``easy'' instances where it converges to an optimal solution faster than in the worst case (in Section~\ref{algo-ineg1}).
%
\begin{WISHFUL}
More formally, this algorithm is running in time within $O(n\delta)\subset O(nL)$ where $\delta\in[1..L]$ measures the difficulty of the instance, and we prove that any algorithm in the algebraic decision tree model will require time within $O(n\delta)$ in the worst case over all instances of size $n$ and parameter $\delta$, which also proves the optimality of both our algorithm and previous ones in the worst case over all instances of size $n$ and parameter $L$ (a result unproven until now).\end{WISHFUL}



\section{Background}
\label{sec:background}



\begin{definition}[Optimal Prefix Free Code~\cite{1952-IRE-AMethodForTheInstructionOfMinimumRedundancyCodes-Huffman}]
Given a set of $n$ weights $\weight{1..n}$ an \textsc{Optimal Prefix Free Code} for $\weight{1..n}$ is (...) 
\end{definition}


A useful value will be the maximal codelength of such a code, which must be defined carefully as a given set of weights can admit various optimal prefix free codes, varying in maximal codelength.
\begin{definition}[Optimal Prefix Free Codelength]
Given a set of $n$ weights $\weight{1..n}$, the \textsc{Optimal Prefix Free Codelength} $\lambda(W)$ is the length of the largest code of an optimal prefix free code for the weights $W$, taking the minimal over all such optimal prefix free codes if there are more than one.
\end{definition}
Luckily enough, the \textsc{Optimal Prefix Free Codelength} can be computed in reasonable time as the algorithm suggested by Huffman~\cite{1952-IRE-AMethodForTheInstructionOfMinimumRedundancyCodes-Huffman} returns a code of Optimal Prefix Free Codelength when breaking ties between internal and external nodes in favor of external nodes.


\begin{definition}[Quasi Uniform Sequence~\cite{2013-TIT-OptimalPrefixCodesForPairsOfGeometricallyDistributedRandomVariables-BassinoClementSeroussiViola}]
A set of $n$ weights $\weight{1..n}$ is a \emph{Quasi Uniform Sequence}~\cite{2013-TIT-OptimalPrefixCodesForPairsOfGeometricallyDistributedRandomVariables-BassinoClementSeroussiViola} if (...)
\end{definition}


\begin{definition}[Optimal Limited Length Prefix Free Code]
Given a set of $n$ weights $\weight{1..n}$ and an integer bound $L>\lg n$, an \textsc{Optimal Limited Length Prefix Free Code} is a \textsc{Optimal Prefix Free Code} for $\weight{1..n}$ such that no codelength is longuer than $L$.
\end{definition}


Larmore and Hirschberg solution for computing \textsc{Optimal Limited Length Prefix Free Code} via reduction to the the \textsc{Coin Collector Problem} is actually solving a more precise version of the problem, taking as parameter a set of $n$ individual bounds $(L_i)_{i\in[1..n]}$:

\begin{definition}[Optimal Separatedly Limited Length Prefix Free Code]
Given an array of $n$ weights $\weight{1..n}$ and an array of integer bounds $L[1..n]$, produce a prefix free code for $\weight{1..n}$ such that the codelength associated to the $i$-th weight of $\weight{1..n}$ is at most $L[i]$.
\end{definition}

\begin{LONG}
Note that those problem are well distinct from \textsc{Huffman Coding with Unequal Letter Costs}.
\begin{TODO}
ADD reference?
\end{TODO}
\end{LONG}


\section{An Improved Algorithm}

We describe the algorithm \texttt{Icing Package-Merge}, which pseudo code is given in Algorithm~\ref{algo-ineg1}.  While computing the same prefix free code as the original \texttt{Package-Merge} algorithm from Larmore and Hirschberg~\cite{1990-AFastaAlgorithmForOptimalLengthLimitedHuffmanCodes-LarmoreHirschberg}, this new algorithm exploits the fact that some elements (be it from the original list or packages) remain invariant (as if they were ``iced'', hence the name of the algorithm, \texttt{Icing Package-Merge}) after a certain point of the computation, and do not need to be further considered during any of the following phases of the algorithm: detecting and managing such elements allows to perform less than the $L$ phases (in time within $O(n)$ each) of the original \texttt{Package-Merge} algorithm, hence the running time improvement.

Let $S$ be the input array of frequencies.
%
The \textsc{Icing Package-Merge} algorithm constructs an array $S_2$, which contains the result of merging $S$ with the packages of $S_2$ from the previous iteration ($S$ at the beginning), along with some extra information regarding the formation of each package.
%
During the whole executions, 
\begin{itemize}
\item the pointer $b$ indicates that the sub-list $S_2[0\ldots (b-1)]$ is fixed forever, while
\item the pointer $a$ indicates that the packages of the segment $S_2[0\ldots (a-1)] $ are already present in $S_2[0 \ldots (b-1)]$.
\end{itemize}

Initially $a=0$ and $b=2$, which indicates that the first two elements of the sorted list $S$ are fixed in their position forever, while there is no package present in $S_2[0\ldots (b-1)]$ for the moment. At each iteration, the algorithm builds the packages corresponding to the elements in $S_2[a\ldots (b-1)]$, which are certified to be fixed forever, and insert these in the list $S_2$, adding the appropriate elements from the list $S_1$, which contains the base elements from $S$ that are not yet in $S_2[0\ldots (b-1)]$, in between.
%
After inserting all of these packages into $S_2$ and changing $b$ and $a$ accordingly (note $a_{next}=b-(b\bmod 2)$), the algorithm merges what remains of $S_1$ (without popping the elements) with the packages of $S_2[b\ldots ]$ and places the resulting list on $S_2[b\ldots ]$. 

Note that at least one new package is classified as ``fixed'' at each iteration (and thus $a$ increases by at least $2$ and $b$ by at least one in each step): when there are less than two elements in $S_2[a\ldots (b-1)]$, then the algorithm declares the first package in line as a fixed element (see the case of the geometric distribution below for a concrete example where this happens).

\begin{algo}{Icing Package-Merge}{S,L}\label{algo-ineg1}
    \IN{A set $S$ of frequencies and an integer $L$ representing the maximum length.}
    \OUT{A map giving the length of the corresponding codeword for each frequency.}
    
    \COM{Two lists $S_1$ and $S_2$ of pairs $(cost,list)$, $S_2$ is the list we are building.}
    \SET{S_1,S_2}{[(s,[s]) : s\in S],[(s,[s]) : s\in S]}
    % \SET{S_1}{[(s,[s]) : s\in S]}
    \CALL{Pop}{S_1}         \CALL{Pop}{S_1}

    \COM{Elements from $S_2[0:b]$ are fixed forever in their positions.}
    \COM{Elements from $S_2[0:a]$ have their packages already in $S_2[0:b]$.}
    \COM{The list $S_1$ corresponds to the elements of $S$ not yet in $S_2[0:b]$.}
    \SET{a,b}{0,2}
    \DOFORI{i}{1}{L-1}      
        \COM{The list $P$ contains the packages of elements in $S_2[a:b]$, which we know to be fixed.}
        \COM{While $Q$ contains the rest of the packages.}
        \SET{P,Q}{\emptyset,\emptyset}
        \DOFORI{j}{a+1}{b-1 \mbox{ step }2}
            \Call{Push}{P,S_2[j-1]+S_2[j]}
        \OD
        \DOFORI{j}{b}{|S_2|-1 \mbox{ step }2}
            \Call{Push}{Q,S_2[j-1]+S_2[j]}
        \OD
        \COM{Make sure the smallest package becomes fixed}
        \IF {P \equiv \emptyset}
          \SET{t}{\CALL{Pop}{Q}}  \Call{Push}{P,t}
        \FI
        % after the merge, eliminate the useless end of S_2
        \COM{After ``package'', eliminate the non-fixed tip.}
        \SET{S_2}{S_2[0\ldots (b-1)]}
        \SET{a}{\max\{a+2,b \mbox{ setting the least significant bit to 0}\}}
        \COM{Find the positions for the elements in $P$.}
        \WHILE{P\neq \emptyset}
            \SET{s}{\CALL{Pop}{P}}
            \WHILE{S_1\neq \emptyset \mbox{ and } S_1[0][0]<s[0]}
                \SET{t}{\CALL{Pop}{S_1}}  \label{elementruns}
                \CALL{Push}{S_2,t}
                \INCR{b}
            \OD
            \CALL{Push}{S_2,s}
            \INCR{b}
        \OD

        % \COM{If the first $2n-2$ elements are fixed, we are done.}
        \IF{b \geq 2n-2 }
            \Call{Break}
        \FI
        \SET{S_2[b\ldots]}{\Call{Merge}{S_1,Q}}
    \OD
    \COM{Take the first $2n-2$ elements from $S_2$.}
    \SET{S_2}{S_2[0\ldots (2n-3)]}
    \SET{code}{\{s : 0 \mbox{ for }s\in S\}}
    \DOFOREACH{(cost,ls)\in S_2}
        \DOFOREACH{s\in ls}
            \INCR{code[s]}
        \OD
    \OD
    \RETURN{code}
\end{algo}

\begin{lemma}[Correctness of \texttt{Icing Package Merge}]
Given a set of $n$ weights $\weight{1..n}$ and an integer bound $L>\lg n$, 
the \texttt{Icing Package Merge} correctly computes 
an \textsc{Optimal Limited Length Prefix Free Code}
for  $\weight{1..n}$  under the constraint $L$.
\end{lemma}
\begin{proof}
We proceed inductively.

Initially all variables $a_0,b_0,S_1^{(0)},S_2^{(0)}$ satisfy the properties (invariants) stated.
Suppose that at stage $i$ we had pointers $a_i$, $b_i$ and lists $S_1^{(i)}$ and $S_2^{(i)}$ satisfying the claimed properties.
\begin{TODO}
Finish the proof?
\end{TODO}
\end{proof}

\begin{lemma}
Given a set of $n$ weights $\weight{1..n}$ and an integer bound $L>\lg n$, 
a variant of the \texttt{Icing Package Merge} correctly computes 
an \textsc{Optimal Limited Length Prefix Free Code}
for $\weight{1..n}$  under the constraint $L$
in space within $O(n)$
\end{lemma}
\begin{proof}
The modifications we describe here do not interfere with the linear space variant in \cite{1990-AFastaAlgorithmForOptimalLengthLimitedHuffmanCodes-LarmoreHirschberg}. Indeed this linear space variant calls the package-merge routine several time, the only difference being that this new package-merge computes (4 integer) side quantities instead of remembering the elements involved in each package.
\end{proof}


\section{Examples}
\label{sec:examples}


We give several examples, showcasing different situations that may arise. Here we just show the evolution of the weights, and forget the labels associated (see the detailed pseudo-algorithm given in Algorithm~\ref{algo-ineg1} or the Python code at \url{https://gitlab.com/FineGrainedAnalysis/BoundedCodeLength}).


\medskip
\begin{example}[Quasi-uniform distribution]
Suppose that $S=[p_1,\ldots,p_n]$, where $p_1<p_2<\ldots<p_n$ as usual, satisfies $p_1+p_2>p_n$. 


In this case the optimal Huffman tree is necessarily a tree with leaves just at levels $d=\lfloor \log_2 n \rfloor$ and (possibly) $d+1$. This property will be apparent in the execution of the algorithm, as $p_1+p_2>p_n$ means that any package is greater than all of the elements in $S$.


Our pointer $b$ advances at once to $n+1$. The packages themselves have a similar quasi-uniform property, hence, in the next step we advance $b$ by at least $n/2$, next by roughly $n/4$ and so on. Getting to $b\geq 2n-2$ in $d$ or $d+1$ steps. 

To show this in a concrete scenario, consider
$$
S = [10,11,12,13,14,15,16,17,18,19]\,.
$$

The evolution of the weights is the following\footnote{$S_2[0\ldots (a-1)]$ is marked in red, $S_2[a\ldots (b-1)]$ in blue, and the new fixed packages are underlined.}:

\begin{tabular}{l}
  {\color{blue} 10, 11},12, 13, 14, 15, 16, 17, 18, 19 \\
  {\color{red}10, 11}, {\color{blue}12, 13, 14, 15, 16, 17, 18, 19, \underline{21}} \\
  {\color{red}10, 11, 12, 13, 14, 15, 16, 17, 18, 19},  {\color{blue}21, \underline{25, 29, 33, 37}} \\
  {\color{red}10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 21, 25, 29, 33}, {\color{blue} 37, \underline{46, 62}} \\
  {\color{red}10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 21, 25, 29, 33, 37,46}, {\color{blue}  62,\underline{83} }\,,
\end{tabular}

and at this point we have $b\geq 2n-2=18$ and we stop (regardless of $L$).
\end{example}

\medskip
\begin{example}[Geometric distribution]
Let us suppose
$$
S = [1,2,4,8,16,32,64,128,256,512]\,.
$$
This is somewhat (it could be worse) the worst case possible for the algorithm as $b$ will advance quite slowly.

We consider $L=5$ for the example:

\begin{tabular}{ll}
    &{\color{red} }{\color{blue} 
1, 2, } 4, 8, 16, 32, 64, 128, 256, 512\\
&{\color{red} 1, 2, }{\color{blue} 
3, } 4, 8, 12, 16, 32, 48, 64, 128, 192, 256, 512, 768\\
&{\color{red} 1, 2, 3, 4, }{\color{blue} 
7, } 8, 16, 20, 32, 48, 64, 112, 128, 256, 320, 512, 768\\
&{\color{red} 1, 2, 3, 4, 7, 8, }{\color{blue} 
15, } 16, 32, 36, 64, 80, 128, 176, 256, 384, 512, 832\\
&{\color{red} 1, 2, 3, 4, 7, 8, 15, 16, }{\color{blue} 
31, } 32, 64, 68, 128, 144, 256, 304, 512, 640
\end{tabular}

The part in black corresponds to the part of $S_2[0\ldots(2n-3)]$ that is not yet fixed.

\end{example}

\medskip
\begin{example}[Mixed case]
Let us consider the example
$$
S = [1,2,4,6,8,20]\,.
$$

In this case we have the following execution for $L=4$. 

\begin{tabular}{ll}
&{\color{red} }{\color{blue} 
1, 2, } 4, 6, 8, 20\\
&{\color{red} 1, 2, }{\color{blue} 
3, } 4, 6, 8, 10, 20, 28\\
&{\color{red} 1, 2, 3, 4, }{\color{blue} 
6, 7, } 8, 14, 20, 30\\
&{\color{red} 1, 2, 3, 4, 6, 7, }{\color{blue} 
8, 13, }20, 22
\end{tabular}

\end{example}

\begin{example}
And here we show a ``randomly'' generated example. Pick the list of frequencies $S=[88, 158, 252, 342, 462, 519, 563, 586, 720, 828]$, with maximum length $L=5$:

\begin{tabular}{ll}
&{\color{red} }{\color{blue} 
88, 158, }252, 342, 462, 519, 563, 586, 720, 828\\
&{\color{red} 88, 158, }{\color{blue} 
246, }252, 342, 462, 519, 563, 586, 594, 720, 828, 981, 1149, 1548\\
&{\color{red} 88, 158, 246, 252, }{\color{blue} 
342, 462, 498, }519, 563, 586, 720, 804, 828, 1082, 1180, 1548, 2130\\
&{\color{red} 88, 158, 246, 252, 342, 462, }{\color{blue} 
498, 519, 563, 586, 720, 804, }828, 1017, 1149, 1524, 1910, 2728\\
&{\color{red} 88, 158, 246, 252, 342, 462, 498, 519, 563, 586, 720, 804, }{\color{blue} 
828, 1017, 1149, 1524, }1845, 2673\,,
\end{tabular}

and here we stop after $L=5$ levels (``fixed'' elements are $17<18=2n-2$ though). Notice that the algorithm advances rapidly when we have elements of similar frequency, as was to be expected. We remark also that stopping because we have at least $2n-2$ elements comes down to ``detecting'' that we already have the optimal Huffman tree.
\end{example}



\section{Fine Grained Analysis}
\label{sec:orgea6add0}

\begin{TODO}
Examples of definitions and results would be as follow:
\begin{definition}[Difficulty]
  Given a set of $n$ positive integer weights $\weight{1..n}$,
  its  \emph{difficulty} $\delta$ is (...)
  \end{definition}
  \begin{lemma}
  Given positive integers $n$ and $L$, and a set of $n$ positive integer weights $\weight{1..n}$ of difficulty $\delta$
  there is an algorithm computing the \textsc{Optimal Limited Length Prefix Free Codes} of $\weight{1..n}$ with length restriction $L$
  in time within $O(\alpha n)$.
  \end{lemma}
  Next step is to prove them!
  \end{TODO}


\section{Computational Lower Bound}
\label{sec:org36d1f47}

\begin{TODO}
The following lower bound would already be a neat result, and seems a logical exercise before proving a more general one.
\begin{lemma}
Consider an algorithm $A$ computing \textsc{Optimal Limited Length Prefix Free Codes} in the algebraic decision tree model.
For any positive integer $n$, there is a set of $n$ positive integer weights $\weight{1..n}$
such that the running time of $A$ on $\weight{1..n}$ with length restriction  \(L=1+\lg n\)
is within \(\Omega(n\lg n)\).
\end{lemma}

The following lower bound should be already known?
\begin{lemma}
Consider an algorithm $A$ computing \textsc{Optimal Limited Length Prefix Free Codes} in the algebraic decision tree model.
For any positive integer $n$, and positive integer $L\in\Theta(\lg n)$,
there is a set of $n$ positive integer weights $\weight{1..n}$ 
such that the running time of $A$ on $\weight{1..n}$ with length restriction $L$
is within \(\Omega(n L)\).
\end{lemma}

Here would be an example of the final result that we are aiming for.
\begin{lemma}
Consider an algorithm $A$ computing \textsc{Optimal Limited Length Prefix Free Codes} in the algebraic decision tree model.
For any positive integer $n$, $L\in\Theta(\lg n)$ and $\alpha\in[1..n-1]$,
there is a set of $n$ positive integer weights $\weight{1..n}$
of difficulty $\delta$
such that the running time of $A$ on $\weight{1..n}$ with length restriction $L$
is within \(\Omega(\alpha n)\).
\end{lemma}
Note that I don't know yet how to prove any of those results!
\end{TODO}


  
\section{Discussion}
\subsubsection*{In this paper,}
\subsubsection*{This is important,}
\subsubsection*{Perspectives:}
\begin{itemize}
\item \textbf{finer constraints (one code length limit per message)}
\item alphabetic binary search tree (with constraints)
\item Dynamic trees
\item D-ary for D>2
\item Computation on Unsorted input (via Deferred Data Structures)
\item Applications to Compressed Data Structures for Permutations
\begin{itemize}
\item bounded height => bounded worst case query time
\end{itemize}
\item Experimentations 
\begin{itemize}
\item permutations from BWT (Construction time, Indexing space, Query Time)
\end{itemize}
\end{itemize}




%%% Local Variables:
%%% mode: latex
%%% TeX-master: "2020-DRAFT-FasterOptimalLimitedLengthPrefixFreeCodes-BarbayLecroqRotondo.tex"
%%% End:
